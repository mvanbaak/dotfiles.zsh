# See if we can use colors.
autoload colors zsh/terminfo
if [[ "$terminfo[colors]" -ge 8 ]]; then
	colors
fi
for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE GREY BLACK; do
	eval PR_$color='%{$terminfo[bold]$fg[${(L)color}]%}'
	eval PR_LIGHT_$color='%{$fg[${(L)color}]%}'
	export PR_$color
	export PR_LIGHT_$color
done
PR_NO_COLOUR="%{$terminfo[sgr0]%}"
export PR_NO_COLOUR

# See if we can use extended characters to look nicer.
typeset -A altchar
set -A altchar ${(s..)terminfo[acsc]}
PR_SET_CHARSET="%{$terminfo[enacs]%}"
PR_SHIFT_IN="%{$terminfo[smacs]%}"
PR_SHIFT_OUT="%{$terminfo[rmacs]%}"
PR_HBAR=${altchar[q]:--}
PR_ULCORNER=${altchar[l]:--}
PR_LLCORNER=${altchar[m]:--}
PR_LRCORNER=${altchar[j]:--}
PR_URCORNER=${altchar[k]:--}

# Clear LSCOLORS
unset LSCOLORS

# Main change, you can see directories on a dark background
export LS_COLORS=gxfxcxdxbxegedabagacad
export LSCOLORS=gxfxcxdxbxegedabagacad
# enable colors on CLI
export CLICOLOR=1
