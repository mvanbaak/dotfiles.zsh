# Changing Directories
setopt auto_cd # If you type foo, and it isn't a command, and it is a directory in your cdpath, go there
setopt auto_pushd # Make cd push the old directory onto the directory stack
setopt cdable_vars # if argument to cd is the name of a parameter whose value is a valid directory, it will become the current directory
setopt NO_chase_dots
setopt NO_chase_links
setopt pushd_ignore_dups # don't push multiple copies of the same directory onto the directory stack
setopt pushd_minus
setopt pushd_silent

# Completion
setopt always_to_end # If a completion is performed with the cursor within a word, and a full completion is inserted, the cursor is moved to the end of the word
setopt NO_auto_name_dirs
setopt NO_complete_aliases # Prevents aliases on the command line from being internally substituted before completion is attempted
setopt complete_in_word # If unset, the cursor is set to the end of the word if completion is started. Otherwise it stays there and completion is done from both ends
setopt glob_complete # When the current word has a glob pattern, do not insert all the words resulting from the expansion but generate matches as for completion and cycle through them like MENU_COMPLETE.
setopt NO_list_beep # DONT Beep on an ambiguous completion
setopt list_packed # Try to make the completion list smaller (occupying less lines) by printing the matches in columns with different widths
setopt NO_list_rows_first # DONT Lay out the matches in completion lists sorted horizontally, that is, the second match is to the right of the first one, not under it as usual.
setopt list_types # When listing files that are possible completions, show the type of each file with a trailing identifying mark

# Expansion and Globbing
setopt brace_ccl # Expand expressions in braces which would not otherwise undergo brace expansion to a lexically ordered list of all the characters
setopt extended_glob # Treat the ‘#’, ‘~’ and ‘^’ characters as part of patterns for filename generation, etc. (An initial unquoted ‘~’ always produces named directory expansion.)
setopt magic_equal_subst # All unquoted arguments of the form ‘anything=expression’ appearing after the command name have filename expansion (that is, where expression has a leading ‘~’ or ‘=’) performed on expression as if it were a parameter assignment
setopt null_glob # If a pattern for filename generation has no matches, delete the pattern from the argument list instead of reporting an error. Overrides NOMATCH
setopt NO_numeric_glob_sort # DONT If numeric filenames are matched by a filename generation pattern, sort the filenames numerically rather than lexicographically

# History
setopt NO_hist_allow_clobber # DONT Add ‘|’ to output redirections in the history
setopt NO_hist_beep # DONT Beep when an attempt is made to access a history entry which isn’t there
setopt hist_expire_dups_first # If the internal history needs to be trimmed to add the current command line, setting this option will cause the oldest history event that has a duplicate to be lost before losing a unique event from the list
setopt hist_find_no_dups # When searching for history entries in the line editor, do not display duplicates of a line previously found, even if the duplicates are not contiguous
setopt hist_ignore_all_dups # If a new command line being added to the history list duplicates an older one, the older command is removed from the list (even if it is not the previous event)
setopt hist_ignore_dups # Do not enter command lines into the history list if they are duplicates of the previous event
setopt hist_ignore_space # Remove command lines from the history list when the first character on the line is a space, or when one of the expanded aliases contains a leading space
setopt hist_no_functions # Remove function definitions from the history list. Note that the function lingers in the internal history until the next command is entered before it vanishes, allowing you to briefly reuse or edit the definition
setopt hist_no_store # Remove the history (fc -l) command from the history list when invoked. Note that the command lingers in the internal history until the next command is entered before it vanishes, allowing you to briefly reuse or edit the line
setopt hist_reduce_blanks # Remove superfluous blanks from each command line being added to the history list
setopt hist_save_no_dups # When writing out the history file, older commands that duplicate newer ones are omitted
setopt hist_verify # Whenever the user enters a line with history expansion, don’t execute the line directly; instead, perform history expansion and reload the line into the editing buffer
setopt inc_append_history # This options works like APPEND_HISTORY except that new history lines are added to the $HISTFILE incrementally (as soon as they are entered), rather than waiting until the shell exits
setopt share_history # This option both imports new commands from the history file, and also causes your typed commands to be appended to the history file

# Initialisation

# Input/Output
setopt NO_correct # DONT Try to correct the spelling of commands
setopt correct_all # Try to correct the spelling of all arguments in a line
setopt dvorak # Use the Dvorak keyboard instead of the standard qwerty keyboard as a basis for examining spelling mistakes for the CORRECT and CORRECT_ALL options and the spell-word editor command
setopt interactive_comments # Allow comments even in interactive shells
setopt mail_warning # Print a warning message if a mail file has been accessed since the shell last checked
setopt NO_path_dirs # DONT Perform a path search even on command names with slashes in them. Thus if ‘/usr/local/bin’ is in the user’s path, and he or she types ‘X11/xinit’, the command ‘/usr/local/bin/X11/xinit’ will be executed
setopt print_eight_bit # Print eight bit characters literally in completion lists, etc
setopt rc_quotes # Allow the character sequence ‘’’’ to signify a single quote within singly quoted strings

# Job Control
setopt NO_auto_resume # DONT Treat single word simple commands without redirection as candidates for resumption of an existing job
setopt NO_hup # DONT Send the HUP signal to running jobs when the shell exits
setopt long_list_jobs # List jobs in the long format by default

# Prompting
setopt prompt_subst # If set, parameter expansion, command substitution and arithmetic expansion are performed in prompts. Substitutions within prompts do not affect the command status

# Scripts and Functions
setopt c_bases # Output hexadecimal numbers in the standard C format, for example ‘0xFF’ instead of the usual ‘16#FF’
setopt multios # Perform implicit tees or cats when multiple redirections are attempted

# Shell Emulation

# Shell State

# Zle
setopt NO_beep # DONT Beep on error in ZLE
