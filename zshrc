source ${HOME}/.zsh/checks.zsh
source ${HOME}/.zsh/colors.zsh
source ${HOME}/.zsh/exports.zsh
source ${HOME}/.zsh/bindkeys.zsh
source ${HOME}/.zsh/setopts.zsh
source ${HOME}/.zsh/aliases.zsh
source ${HOME}/.zsh/completion.zsh
source ${HOME}/.zsh/functions.zsh

## use hard limits, except for a smaller stack and no core dumps
unlimit
limit stack 8192
limit core 0
limit -s

# by default, we want this to get set.
# Even for non-interactive, non-login shells.
if [ "`id -gn`" = "`id -un`" -a `id -u` -gt 99 ]; then
        umask 002
  else
        umask 022
fi

## The file to save the history in when an interactive shell exits.
## If unset, the history is not saved.
HISTFILE=${HOME}/.zsh_history

## The maximum number of events stored in the internal history list.
HISTSIZE=5000

## The maximum number of history events to save in the history file.
SAVEHIST=4500

## maximum size of the directory stack.
DIRSTACKSIZE=20

## file for mail checking
MAIL=/var/mail/$USERNAME

## The interval in seconds between checks for new mail.
MAILCHECK=30

## The interval in seconds between checks for login/logout activity
## using the watch parameter.
LOGCHECK=30

## auto logout after timeout in seconds
#TMOUT=1800

## if we are in X then disable TMOUT
case $TERM in
	*xterm*|rxvt|(dt|k|E)term)
	unset TMOUT
	;;
esac

## This is a multiple move based on zsh pattern matching.  To get the full
## power of it, you need a postgraduate degree in zsh.
## Read /path_to_zsh_functions/zmv for some basic examples.
autoload -U zmv

## watch for my friends
## An array (colon-separated list) of login/logout events to report.
## If it contains the single word `all', then all login/logout events
## are reported.  If it contains the single word `notme', then all
## events are reported as with `all' except $USERNAME.
## An entry in this list may consist of a username,
## an `@' followed by a remote hostname,
## and a `%' followed by a line (tty).
#watch=( $(<~/.friends) )  ## watch for people in $HOME/.friends file
watch=(notme)  ## watch for everybody but me
LOGCHECK=60  ## check every ... seconds for login/logout activity

## The format of login/logout reports if the watch parameter is set.
## Default is `%n has %a %l from %m'.
## Recognizes the following escape sequences:
## %n = name of the user that logged in/out.
## %a = observed action, i.e. "logged on" or "logged off".
## %l = line (tty) the user is logged in on.
## %M = full hostname of the remote host.
## %m = hostname up to the first `.'.
## %t or %@ = time, in 12-hour, am/pm format.
## %w = date in `day-dd' format.
## %W = date in `mm/dd/yy' format.
## %D = date in `yy-mm-dd' format.
WATCHFMT='%n %a %l from %m at %t.'

## functions for displaying neat stuff in *term title
case $TERM in
	*xterm*|rxvt|(dt|k|E)term)
	## display user@host and full dir in *term title
	precmd () {
		print -Pn  "\033]0;%n@%m %~\007"
		#print -Pn "\033]0;%n@%m%#  %~ %l  %w :: %T\a" ## or use this
	}
	## display user@host and name of current process in *term title
	preexec () {
		print -Pn "\033]0;%n@%m <$1> %~\007"
		#print -Pn "\033]0;%n@%m%#  <$1>  %~ %l  %w :: %T\a" ## or use this
	}
	;;
esac

# source private zshrc for user specific configurations
if [[ -f ${HOME}/.zshrc.private ]]
then
    source ${HOME}/.zshrc.private
fi

source ${HOME}/.zsh/prompt.zsh
