# Show a character indicating vcs system in the prompt
function prompt_char {
    git branch >/dev/null 2>/dev/null && echo ' ±' && return
    hg root >/dev/null 2>/dev/null && echo ' ☿' && return
    svn info >/dev/null 2>/dev/null && echo ' ⚡' && return
    echo ''
}

# Show count of stashed changes
function +vi-git-stash() {
    local -a stashes

    if [[ -s ${hook_com[base]}/.git/refs/stash ]] ; then
        stashes=$(git stash list 2>/dev/null | wc -l | tr -d ' ')
        hook_com[misc]+=" (${stashes} stashed)"
    fi
}

# Show remote ref name and number of commits ahead-of or behind
function +vi-git-st() {
    local ahead behind remote
    local -a gitstatus

    # Are we on a remote-tracking branch?
    remote=${$(git rev-parse --verify ${hook_com[branch]}@{upstream} \
        --symbolic-full-name 2>/dev/null)/refs\/remotes\/}

    if [[ -n ${remote} ]] ; then
        # for git prior to 1.7
        # ahead=$(git rev-list origin/${hook_com[branch]}..HEAD | wc -l)
        ahead=$(git rev-list ${hook_com[branch]}@{upstream}..HEAD 2>/dev/null | wc -l | tr -d ' ')
        (( $ahead )) && gitstatus+=( "${PR_GREEN}+${ahead}${PR_GREY}" )

        # for git prior to 1.7
        # behind=$(git rev-list HEAD..origin/${hook_com[branch]} | wc -l)
        behind=$(git rev-list HEAD..${hook_com[branch]}@{upstream} 2>/dev/null | wc -l | tr -d ' ')
        (( $behind )) && gitstatus+=( "${PR_RED}-${behind}${PR_GREY}" )

        hook_com[branch]="${hook_com[branch]} -> ${PR_WHITE}[${remote} ${(j:/:)gitstatus}${PR_WHITE}]${PR_GREY}"
    fi
}

# Show svn modified and non-tracked items as 'staged' and 'unstaged'. Yes, this is ugly but it works
function +vi-svn-st() {
    local locallychanged untracked

    locallychanged=${$(svn st | grep '[M|A]' | wc -l | tr -d ' ')}
    untracked=${$(svn st | grep '?' | wc -l | tr -d ' ')}

    (( locallychanged )) && hook_com[staged]=${PR_YELLOW}U${PR_GREY}
    (( untracked )) && hook_com[unstaged]=${PR_RED}?${PR_GREY}
}

# Our battery indicator
function battery_charge {
    if [[ -f ~/bin/batstatus.py && -x ~/bin/batstatus.py ]]; then
        echo `~/bin/batstatus.py` 2>/dev/null
    else
        echo ''
    fi
}

# Show python virtualenv info
function python_virtualenv_info {
    [ $VIRTUAL_ENV ] && echo "%{${fg_bold[white]}%}(env: %{${fg[green]}%}`basename \"$VIRTUAL_ENV\"`%{${fg_bold[white]}%})%{${reset_color}%}"
}

# Show terraform info
function tf_prompt_info() {
    # dont show 'default' workspace in home dir
    [[ "$PWD" == ~ ]] && return
    # check if in terraform dir
    if [ -d .terraform ]; then
        # Most of scripts uses 'terraform workspace show' but it's too slow in some cases.
        # We can just bring out the environment file to bash
        if [ -f .terraform/environment ]; then
            workspace=$(cat .terraform/environment 2> /dev/null) || return
            echo " [${workspace}]"
        else
            echo " [default]"
        fi
    fi
}

# precmd - run this every time the prompt is shown
function precmd {
    local TERMWIDTH
    (( TERMWIDTH = ${COLUMNS} - 1 ))

    # Truncate the path if it's too long.
    PR_FILLBAR=""
    PR_PWDLEN=""
    PR_VCSFILL=""

    local promptsize=${#${(%):---(%m:%l)---()--}}
    local pwdsize=${#${(%):-%~}}

    if [[ "$promptsize + $pwdsize" -gt $TERMWIDTH ]]; then
        ((PR_PWDLEN=$TERMWIDTH - $promptsize))
    else
        PR_FILLBAR="\${(l.(($TERMWIDTH - ($promptsize + $pwdsize)))..${PR_HBAR}.)}"
    fi

    # See if we can use vcs_info
    autoload vcs_info
    zstyle ':vcs_info:*' enable git svn
    zstyle ':vcs_info:*' get-revision true
    zstyle ':vcs_info:*' check-for-changes true
    zstyle ':vcs_info:*' branchformat "%b"

    # svn specific configs
    zstyle ':vcs_info:svn*' formats " (%s)-[%b:%i] %c%u %m"
    zstyle ':vcs_info:svn*:*' stagedstr "${PR_ORANGE}U${PR_GREY}"
    zstyle ':vcs_info:svn*:*' unstagedstr "${PR_RED}?${PR_GREY}"
    zstyle ':vcs_info:svn*+set-message:*' hooks svn-st

    # git specific configs
    zstyle ':vcs_info:git*' formats " (%s)-[%b:%12.12i] ${PR_GREEN}%c${PR_RED}%u${PR_GREY} %m"
    zstyle ':vcs_info:git*' actionformats "(%s|${PR_WHITE}%a${PR_GREY}) %12.12i %c%u %b%m"
    zstyle ':vcs_info:git*:*' stagedstr "${PR_GREEN}S${PR_GREY}"
    zstyle ':vcs_info:git*:*' unstagedstr "${PR_RED}U${PR_GREY}"
    zstyle ':vcs_info:git*+set-message:*' hooks git-st git-stash

    # fill the vcs_info_msg_0_ variable
    vcs_info

    if [[ -n ${vcs_info_msg_0_} ]]; then
        PR_VCSINFO='${PR_CYAN}|${PR_GREY}${vcs_info_msg_0_}
'
    else
        PR_VCSINFO=''
    fi
}

setopt extended_glob
preexec () {
    if [[ "$TERM" == "screen" ]]; then
        local CMD=${1[(wr)^(*=*|sudo|-*)]}
        echo -n "\ek$CMD\e\\"
    fi
}

setprompt () {
    # Need this so the prompt will work.

    # Decide if we need to set titlebar text.
    case $TERM in
    xterm*)
        PR_TITLEBAR=$'%{\e]0;%(!.-=*[ROOT]*=- | .)%n@%m:%~ | ${COLUMNS}x${LINES} | %y\a%}'
        ;;
    screen)
        PR_TITLEBAR=$'%{\e_screen \005 (\005t) | %(!.-=[ROOT]=- | .)%n@%m:%~ | ${COLUMNS}x${LINES} | %y\e\\%}'
        ;;
    *)
        PR_TITLEBAR=''
        ;;
    esac

    # Decide whether to set a screen title
    if [[ "$TERM" == "screen" ]]; then
        PR_STITLE=$'%{\ekzsh\e\\%}'
    else
        PR_STITLE=''
    fi

    # Finally, the prompt.
    PROMPT='${PR_SET_CHARSET}${PR_STITLE}${(e)PR_TITLEBAR}${PR_CYAN}${PR_SHIFT_IN}${PR_ULCORNER}\
${PR_BLUE}${PR_HBAR}${PR_SHIFT_OUT}(${PR_GREEN}%m:%l$PR_BLUE)\
${PR_SHIFT_IN}${PR_HBAR}${PR_CYAN}${PR_HBAR}${(e)PR_FILLBAR}\
${PR_BLUE}${PR_HBAR}${PR_SHIFT_OUT}(${PR_MAGENTA}%${PR_PWDLEN}<...<%~%<<${PR_BLUE})\
${PR_SHIFT_IN}${PR_HBAR}${PR_CYAN}${PR_URCORNER}${PR_SHIFT_OUT}\

${(e)PR_VCSINFO}\
${PR_CYAN}${PR_SHIFT_IN}${PR_LLCORNER}\
${PR_BLUE}${PR_HBAR}${PR_SHIFT_OUT}(%(?..${PR_LIGHT_RED}%?${PR_BLUE}:)${PR_GREEN}%(!.%SROOT%s.%n)${PR_BLUE})\
${PR_SHIFT_IN}${PR_HBAR}${PR_SHIFT_OUT}${PR_CYAN}${PR_SHIFT_IN}${PR_HBAR}${PR_SHIFT_OUT}\
$(tf_prompt_info)\
${PR_NO_COLOUR}$(prompt_char) '

    RPROMPT='$(python_virtualenv_info)${PR_CYAN}${PR_SHIFT_IN}${PR_HBAR}${PR_BLUE}${PR_HBAR}${PR_SHIFT_OUT}(${PR_YELLOW}%D{%F %H:%M}${PR_BLUE})${PR_SHIFT_IN}${PR_HBAR}${PR_CYAN}${PR_LRCORNER}${PR_SHIFT_OUT}${PR_NO_COLOUR}'

    PS2='${PR_CYAN}${PR_SHIFT_IN}${PR_HBAR}${PR_SHIFT_OUT}\
${PR_BLUE}${PR_SHIFT_IN}${PR_HBAR}${PR_SHIFT_OUT}(\
${PR_LIGHT_GREEN}%_${PR_BLUE})${PR_SHIFT_IN}${PR_HBAR}${PR_SHIFT_OUT}\
${PR_CYAN}${PR_SHIFT_IN}${PR_HBAR}${PR_SHIFT_OUT}${PR_NO_COLOUR} '
}

# run the function for our prompt
setprompt

## SPROMPT - the spelling prompt
SPROMPT='zsh: correct '%R' to '%r' ? ([Y]es/[N]o/[E]dit/[A]bort) '
