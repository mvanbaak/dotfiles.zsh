# Some checks to set variables.
# These variables make life easier in the rest of the configs
IS_MAC=0
if [[ $(uname) = 'Darwin' ]]
then
	IS_MAC=1
fi

IS_FREEBSD=0
if [[ $(uname) = 'FreeBSD' ]]
then
	IS_FREEBSD=1
fi

IS_LINUX=0
if [[ $(uname) = 'Linux' ]]
then
	IS_LINUX=1
fi

HAS_BREW=0
if [[ -x `which brew` ]]
then
	HAS_BREW=1
fi

HAS_APT=0
if [[ -x `which aptitude` ]]
then
	HAS_APT=1
fi

IS_ROOT=0
if (( EUID == 0 ))
then
	IS_ROOT=1
fi
