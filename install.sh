#!/bin/sh

# Setup zsh

# Fix ~/.zshrc
if [ -f ~/.zshrc ]
then
    mv ~/.zshrc ~/.zshrc.backup.mvanbaak
fi
echo "source ~/.zsh/zshrc" > ~/.zshrc

# Fix ~/.zshenv
if [ -f ~/.zshenv ]
then
    mv ~/.zshenv ~/.zshenv.backup.mvanbaak
fi
echo "source ~/.zsh/zshenv" > ~/.zshenv
