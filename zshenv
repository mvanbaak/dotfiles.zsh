# Mac OS X uses path_helper and /etc/paths.d to preload PATH, clear it out first
if [ -x /usr/libexec/path_helper ]; then
	PATH=''
	eval `/usr/libexec/path_helper -s`
fi

## set path and cdpath
path=($path /bin /usr/bin /usr/X11R6/bin)
path=($path /usr/local/bin $HOME/bin)
path=($path /opt/homebrew/bin)
path=($path /opt/subversion/bin)
path=($path /opt/local/bin /opt/local/sbin)
path=($path /opt/local/apache2/bin)
path=($path /usr/local/mysql/bin)
path=($path /usr/local/sbin)
path=($path $HOME/pear/bin)
path=($path $HOME/.composer/vendor/bin)
path=($path /opt/homebrew/opt/rclone-mac/libexec/rclone)
cdpath=(~ ..) ## on cd command offer dirs in home and one dir up.

## for root add sbin dirs to path
if (( EUID == 0 )); then
	path=($path /sbin /usr/sbin /usr/local/sbin)
fi

export PATH
