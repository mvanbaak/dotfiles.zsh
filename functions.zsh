## csh compatibility
setenv () { typeset -x "${1}${1:+=}${(@)argv[2,$#]}" }

## find process to kill and kill it.
pskill ()
{ 
	local pid
	pid=$(ps -ax | grep $1 | grep -v grep | awk '{ print $1 }')
	echo -n "killing $1 (process $pid)..."
	kill -9 $=pid
	echo "slaughtered."
}

## invoke this every time when u change .zshrc to
## recompile it.
src ()
{
	autoload -U zrecompile
	[ -f ${HOME}/.zshrc ] && zrecompile -p ${HOME}/.zshrc
	[ -f ${HOME}/.zcompdump ] && zrecompile -p ${HOME}/.zcompdump
	[ -f ${HOME}/.zshrc.zwc.old ] && rm -f ${HOME}/.zshrc.zwc.old
	[ -f ${HOME}/.zcompdump.zwc.old ] && rm -f ${HOME}/.zcompdump.zwc.old
	source ${HOME}/.zshrc
}

## find all suid files
suidfind ()
{
	ls -l /**/*(su0x)
}

## restore all .bak files
restore_bak ()
{
	autoload -U zmv
	zmv '(**/)(*).bak' '$1$2'
}

## display processes tree in less
pst ()
{
	pstree -p $* | less -S
}

## search for various types or README file in dir and display them in $PAGER
readme ()
{
	local files
	files=(./(#i)*(read*me|lue*m(in|)ut)*(ND))
	if (($#files))
	then 
		$PAGER $files
	else
		print 'No README files.'
	fi
}

mcdrom ()
{
	local mounted
	local cpwd
	mounted=$(grep cdrom /etc/mtab)
	if [[ $mounted = "" ]]
	then
		mount /cdrom
		echo "-- mounted cdrom --"
		cd /cdrom ; ls
	else
		cpwd=$(pwd|grep cdrom)
		if [[ $cpwd = "" ]];then
			umount /cdrom
			echo "-- umounted cdrom --"
		else
			cd;umount /cdrom
			echo "-- umounted cdrom --"
			pwd
		fi
	fi
}

## Use vim to convert plaintext to HTML
function 2html()
{ vim -n -c ':so $VIMRUNTIME/syntax/2html.vim' -c ':wqa' $1 > /dev/null 2> /dev/null }

## Get the AWS account id for one of the configured profiles
function get_aws_account_id() {
    aws --profile "$1" sts get-caller-identity --output text --query 'Account'
}
