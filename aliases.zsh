## aliases ####
alias p='ps -fu $USER'
alias v='less'
alias h='history'
alias z='vim ~/.zshrc;src'
alias gvim='gvim -U ~/.gvimrc'
alias g='gvim'
alias vi='vim'
alias mv='nocorrect mv -i'
alias cp='nocorrect cp -i'
alias rm='nocorrect rm -i'
alias mkdir='nocorrect mkdir'
alias man='nocorrect man'
alias find='noglob find'
#alias ls='ls --color=auto'
alias l='ls'
alias ll='ls -l'
alias l.='ls -A'
alias ll.='ls -al'
alias lsa='ls -ls .*' ## list only file beginning with "."
alias lsd='ls -ld *(-/DN)' ## list only dirs
alias du1='du -hs *(/)' ## du with depth 1
alias u='uptime'
alias j='ps ax'
alias ..='cd ..'
alias cd/='cd /'
## global aliases, this is not good but it's useful
alias -g L='|less'
alias -g G='|grep'
alias -g T='|tail'
alias -g H='|head'
alias -g W='|wc -l'
alias -g S='|sort'
alias -g US='|sort -u'
alias -g NS='|sort -n'
alias -g RNS='|sort -nr'
alias -g N='&>/dev/null&'


