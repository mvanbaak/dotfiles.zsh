## (( ${+*} )) = if variable is set don't set it anymore
(( ${+USER} )) || export USER=$USERNAME
(( ${+HOSTNAME} )) || export HOSTNAME=$HOST
(( ${+EDITOR} )) || export EDITOR=`which vim`
(( ${+VISUAL} )) || export VISUAL=`which vim`
(( ${+FCEDIT} )) || export FCEDIT=`which vim`
(( ${+PAGER} )) || export PAGER=`which less`
(( ${+MAILCALL} )) || export MAILCALL='*** NEW MAIL ***' ## new mail warning
(( ${+LESSCHARSET} )) || export LESSCHARSET='latin1' ## charset for pager
(( ${+LESSOPEN} )) || export LESSOPEN='|lesspipe.sh %s'
(( ${+CC} )) || export CC='gcc' ## or egcs or whatever
(( ${+LC_ALL} )) || export LC_ALL='en_US.UTF-8'

# some things we just want to set, no matter what
#export TERM=xterm-256color
# CTAGS Sorting in VIM/Emacs is better behaved with this in place
export LC_COLLATE=C
# Our prompt.sh already shows the virtualenv if needed
export VIRTUAL_ENV_DISABLE_PROMPT=1
# get rid of docker CLI ads
export DOCKER_CLI_HINTS=false
